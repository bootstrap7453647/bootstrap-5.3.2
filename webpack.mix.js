// webpack.mix.js

let mix = require('laravel-mix');

mix.js('src/js/min.js', 'public/js')
    .sass('src/styles/min.scss', 'css')
    .setPublicPath('public');

mix.combine([
    "node_modules/animate.css/animate.css",
], "public/css/vendor.css");